# Dashing setup:

`$ gem install dashing`

`$ bundle`

Set-up env variable for your gitlab token.
This is located in Profile -> Account -> private token

`$ export GITLAB_TOKEN="Your Gitlab token here"`

Startup Dashing

`$ dashing start`

Your dashboard should be running on localhost:3030


