require 'open-uri'
require 'json'

# Public: Responsible for retrieving the status of the most recent build
# of master for the passed GitLab project id in a format expected by the
# widget.
#
# Examples
#
#   GitlabBuild.call(12345)
#   # => { status: 'success', build_id: '42', commit_message: 'Forgot Towel' }
class GitlabBuild
  def self.call(project_id)
    new(project_id).call
  end

  def initialize(project_id)
    self.project_id = project_id
  end

  def call
    {
      status: status,
      build_id: id,
      commit_message: commit_message
    }
  end

  private

  attr_accessor :project_id

  def status
    current_build['status']
  end

  def id
    current_build['id']
  end

  def commit_message
    current_build['commit']['message']
  end

  def current_build
    @current_build ||= project_builds_json.find { |build|
      build['ref'] == 'master'
    }
  end

  def project_builds_json
    @project_builds_json ||= JSON.parse project_builds_response
  end

  def project_builds_response
    @project_builds_response ||= open(build_url).read
  end

  def build_url
    format 'https://gitlab.com/api/v3/projects/%s/builds?private_token=%s',
           project_id,
           'VtEiTv4T9xtxxG1xMW-c'
  end
end
