require_relative '../lib/gitlab_build'

SCHEDULER.every '10s', first_in: 0 do
  send_event 'adlm_workstation_gitlab_build',
             GitlabBuild.call(1090430)
end
