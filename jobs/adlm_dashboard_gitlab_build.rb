require_relative '../lib/gitlab_build'

SCHEDULER.every '10s', first_in: 0 do
  send_event 'adlm_dashboard_gitlab_build',
             GitlabBuild.call(1100264)
end
