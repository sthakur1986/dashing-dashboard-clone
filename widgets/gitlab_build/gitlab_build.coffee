class Dashing.GitlabBuild extends Dashing.Widget
  onData: (data) ->
    $(@node).find('.project span').text(this.project)
    $(@node).find('.status').text(data.status)
    $(@node).find('.commit_message').text(data.commit_message)
    $(@node).attr('data-status', data.status)



