require_relative '../test_helper'
require_relative '../../lib/gitlab_build'

describe GitlabBuild do
  before do
    @build = GitlabBuild.new 1090430
  end

  describe '#call' do
    it 'returns the current build status' do
      stubbed_server_response do
        assert_equal 'success', @build.call[:status]
      end
    end

    it 'returns the current build id' do
      stubbed_server_response do
        assert_equal 1222912, @build.call[:build_id]
      end
    end

    it 'returns the commit message of the commit that triggered the build' do
      stubbed_server_response do
        assert_equal 'Change docker recipe to use proxy_port instead of ' \
                     "proxy_host twice\n",
                     @build.call[:commit_message]
      end
    end
  end

  private

  def stubbed_server_response
    data_path = File.join __dir__, '..', 'data', 'adlm_workstation_builds.json'
    data = File.open(data_path).read

    @build.stub :project_builds_response, data do
      yield
    end
  end
end
